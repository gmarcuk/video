<?php

namespace app\controllers;

use Yii;
use app\models\Comment;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CommentController implements the CRUD actions for Comment model.
 */
class CommentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['GET'],
                ],
            ],
        ];
    }

    /**
     * Updates an existing Comment model.
     * If update is successful, the browser will be redirected to the 'view' page of related video with anchor to comment.
     * Only available to author of the comment.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->getIsGuest()) {
            //not logged in
            return $this->redirect(['user/login']);
        }

        $model = $this->findModel($id);

        if (Yii::$app->user->identity->id != $model->user_id) {
            // is admin
            return $this->redirect(['video/index']);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $url = Yii::$app->urlManager->createUrl('video/view/'.$model->video_id."#comment".$id);
            return $this->redirect($url);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Comment model.
     * The browser will be redirected to the page request came from.
     * Only available to admins or author of the comment.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->getIsGuest()) {
            //not logged in
            return $this->redirect(['user/login']);
        }

        $model = $this->findModel($id);

        if (!Yii::$app->user->identity->isAdmin() && Yii::$app->user->identity->id != $model->user_id) {
            // is admin
            return $this->goBack();
        }

        $model->delete();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the Comment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Comment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Comment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
