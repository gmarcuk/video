<?php

namespace app\controllers;

use Yii;
use app\models\Video;
use app\models\VideoSearch;
use app\models\User;
use app\models\Comment;
use app\models\Rating;
use app\models\Favorite;
use app\models\Visit;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * VideoController implements the CRUD actions for Video model.
 */
class VideoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['GET'],
                ],
            ],
        ];
    }

    /**
     * Lists all Video models ordered from newest one along with visits models.
     * Sets title to "Newest videos" and displays dashboard
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->getIsGuest()) {
            //not logged in
            return $this->redirect(['user/login']);
        }

        $title = "Newest videos";

        // videos
        $videomodel = new Video();
        $videos = $videomodel->find()->orderBy(['date' => SORT_DESC])->all();

        // users
        $usermodel = new User();
        $users = $usermodel->find()->all();

        // visits
        $visitmodel = new Visit();
        $visits = $visitmodel->find()->all();



        // dashboard
        $dashboard = new \stdClass();
        $dashboard->videos = count($videomodel->find()->where(['user_id' => Yii::$app->user->id])->all());
        $dashboard->comments = count(Comment::find()->where(['user_id' => Yii::$app->user->id])->all());
        $dashboard->favorited = count(Favorite::find()->where(['user_id' => Yii::$app->user->id])->all());
        $dashboard->rated = count(Rating::find()->where(['user_id' => Yii::$app->user->id])->all());

        return $this->render('index', [
            'videos' => $videos,
            'users' => $users,
            'visits' => $visits,
            'title' => $title,
            'dashboard' => $dashboard,
        ]);
    }

    /**
     * Lists all Video models created by logged in user along with visits models.
     * Sets title to "My videos"
     * @return mixed
     */
    public function actionMy()
    {
        if (Yii::$app->user->getIsGuest()) {
            //not logged in
            return $this->redirect(['user/login']);
        }

        $title = "My videos";

        // videos
        $videomodel = new Video();
        $videos = $videomodel->find()->where(['user_id' => Yii::$app->user->id])->all();

        // users
        $usermodel = new User();
        $users = $usermodel->find()->all();

        // visits
        $visitmodel = new Visit();
        $visits = $visitmodel->find()->all();

        return $this->render('index', [
            'videos' => $videos,
            'users' => $users,
            'visits' => $visits,
            'title' => $title
        ]);
    }

    /**
     * Lists all Video models favorited by logged in user along with visits models.
     * Sets title to "Favorited videos"
     * @return mixed
     */
    public function actionFavorites()
    {
        if (Yii::$app->user->getIsGuest()) {
            //not logged in
            return $this->redirect(['user/login']);
        }

        $title = "Favorited videos";

        // videos
        $videomodel = new Video();
        $videos = $videomodel->find()->all();

        // users
        $usermodel = new User();
        $users = $usermodel->find()->all();

        // visits
        $visitmodel = new Visit();
        $visits = $visitmodel->find()->all();

        // favorites
        $favoritemodel = new Favorite();
        $favorites = $favoritemodel->find()->where(['user_id' => Yii::$app->user->id])->all();

        $filteredVideos = [];
        foreach ($favorites as $favorite) {
            $filteredVideos[] = $videomodel->find()->where(['id' => $favorite->video_id])->one();
        }

        return $this->render('index', [
            'videos' => $filteredVideos,
            'users' => $users,
            'visits' => $visits,
            'title' => $title
        ]);
    }

    /**
     * Lists videos based on searched words and displays it in case searched string is found in title, description or label of the video.
     * Also displays it if the searched string is found in name of author of video.
     * @return mixed
     */
    public function actionSearch()
    {
        if (Yii::$app->user->getIsGuest()) {
            //not logged in
            return $this->redirect(['user/login']);
        }

        if (Yii::$app->request->get() && Yii::$app->request->get()['query'] !== "") {
            $query = Yii::$app->request->get()['query'];

            $title = 'Searched for words "'.$query.'"';

            // videos
            $videomodel = new Video();
            $videos = $videomodel->find()->all();

            // users
            $usermodel = new User();
            $users = $usermodel->find()->all();

            // visits
            $visitmodel = new Visit();
            $visits = $visitmodel->find()->all();

            // filter of videos based on query
            $filteredVideos = [];

            foreach ($videos as $video) {
                foreach ($users as $user) {
                    if ($user->user_id == $video->user_id) {
                        $video_user = $user;
                        break;
                    }
                }

                if (stripos($video_user->getName(), $query) !== false) {
                    $filteredVideos[] = $video;
                } else  if (stripos($video->title, $query) !== false) {
                    $filteredVideos[] = $video;
                } else  if (stripos($video->description, $query) !== false) {
                    $filteredVideos[] = $video;
                } else  if (stripos($video->labels, $query) !== false) {
                    $filteredVideos[] = $video;
                }
            }

            return $this->render('index', [
                'videos' => $filteredVideos,
                'users' => $users,
                'visits' => $visits,
                'title' => $title,
                'query' => $query
            ]);
        } else {
            return $this->redirect(['video/index']);
        }
    }

    /**
     * Lists all Video models with label.
     * @param string $id - name of the label
     * @return mixed
     */
    public function actionLabel($id)
    {
        if (Yii::$app->user->getIsGuest()) {
            //not logged in
            return $this->redirect(['user/login']);
        }

        $title = ucfirst($id);

        // videos
        $videomodel = new Video();
        $videos = $videomodel->find()->where(['labels' => $id])->all();

        // users
        $usermodel = new User();
        $users = $usermodel->find()->all();

        // visits
        $visitmodel = new Visit();
        $visits = $visitmodel->find()->all();

        return $this->render('index', [
            'videos' => $videos,
            'users' => $users,
            'visits' => $visits,
            'title' => $title
        ]);
    }

    /**
     * Displays a single Video model along with Favorite models, Comment models and Rating models related to it. All passes all users and headers of the linked url of video.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (Yii::$app->user->getIsGuest()) {
            //not logged in
            return $this->redirect(['user/login']);
        }

        if (Yii::$app->request->isGet) {

            // view video
            $model = $this->findModel($id);
            $user = User::findIdentity($model->user_id);

            // comment model
            $comment = new Comment();

            // get comments
            $comments = $comment->find()->where(['video_id' => $id])->orderBy(['time' => SORT_DESC])->all();

            // get rating
            $rating = new \stdClass();
            $rating->positive = count(Rating::find()->where(['video_id' => $id, 'positive' => true])->all());
            $rating->negative = count(Rating::find()->where(['video_id' => $id, 'positive' => false])->all());

            // favorite
            $favorite = Favorite::find()->where(['video_id' => $id, 'user_id' => Yii::$app->user->id])->one();

            // visit
            $add_visit = new Visit();
            $all_visits = $add_visit->find()->where(['video_id' => $id, 'user_id' => Yii::$app->user->id])->all();
            $visits = count($all_visits);

            // users
            $usermodel = new User();
            $users = $usermodel->find()->all();

            if($visits == 0 || strtotime(end($all_visits)->time) <= time() - (10*60)) {
                // after 10 minutes it is counted as new view
                $add_visit->user_id = Yii::$app->user->id;
                $add_visit->video_id = $id;
                $add_visit->save();
            }

            $visits = count($add_visit->find()->where(['video_id' => $id])->all());
            //$addVisit = Visit::find()->where(['video_id' => $id, 'user_id' => Yii::$app->user->id])->one()

            // get header of url
            $headers = get_headers($model->url);

            return $this->render('view', [
                'model' => $model,
                'user' => $user,
                'users' => $users,
                'headers' => $headers,
                'commentModel' => $comment,
                'comments' => $comments,
                'rating' => $rating,
                'favorite' => $favorite,
                'visits' => $visits
            ]);
        } else if (Yii::$app->request->post()) {
            // create comment
            $post = Yii::$app->request->post();
            $post["Comment"]["video_id"] = $id;
            $model = new Comment();
            $model->load($post) && $model->save();
            return $this->refresh();
        }
    }

    /**
     * Creates a new Video model.
     * If creation is successful, the browser will be redirected to the 'view' page of the video.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->getIsGuest()) {
            //not logged in
            return $this->redirect(['user/login']);
        }

        $model = new Video();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'labels' => $model->rules()[4]['range'],
            ]);
        }

    }

    /**
     * Updates an existing Video model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->getIsGuest()) {
            //not logged in
            return $this->redirect(['user/login']);
        }

        $model = $this->findModel($id);

        if (!Yii::$app->user->identity->isAdmin() && Yii::$app->user->identity->id != $model->user_id) {
            // is admin
            return $this->redirect(['index']);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'labels' => $model->rules()[4]['range'],
            ]);
        }
    }

    /**
     * Deletes an existing Video model along with all related Favorite models, Comment models.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->getIsGuest()) {
            //not logged in
            return $this->redirect(['user/login']);
        }

        $model = $this->findModel($id);

        if (!Yii::$app->user->identity->isAdmin() && Yii::$app->user->identity->id != $model->user_id) {
            // is admin
            return $this->redirect(['index']);
        }

        $model->delete();

        // remove favorites
        $favoritemodel = new Favorite();
        $favorites = $favoritemodel->find()->where(['video_id' => $id])->all();
        foreach($favorites as $favorite) {
            $favorite->delete();
        }

        // remove favorites
        $commentmodel = new Comment();
        $comments = $commentmodel->find()->where(['video_id' => $id])->all();
        foreach($comments as $comment) {
            $comment->delete();
        }

        return $this->redirect(['index']);
    }

    /**
     * Creates new Rating model with positive attribute set to true and relates it to video with passed id.
     * In case of existence of Rating model with relation to video and logged in user, it is update.
     * @param integer $id
     * @return mixed
     */
    public function actionLike($id)
    {
        if (Yii::$app->user->getIsGuest()) {
            //not logged in
            return $this->redirect(['user/login']);
        }

        $rating = new Rating();

        $like["Rating"]["video_id"] = $id;
        $like["Rating"]["user_id"] = Yii::$app->user->id;
        $like["Rating"]["positive"] = true;

        $likes = $rating->find()->where(['video_id' => $id, 'user_id' => Yii::$app->user->id])->all();

        if (!empty($likes)) {
            $likes[0]->positive = true;
            $likes[0]->save();
            return $this->redirect(['video/'.$id]);
        } else {
            $rating->load($like) && $rating->save();
            return $this->redirect(['video/'.$id]);
        }
    }

    /**
     * Creates new Rating model with positive attribute set to false and relates it to video with passed id.
     * In case of existence of Rating model with relation to video and logged in user, it is update.
     * @param integer $id
     * @return mixed
     */
    public function actionDislike($id)
    {
        if (Yii::$app->user->getIsGuest()) {
            //not logged in
            return $this->redirect(['user/login']);
        }

        $rating = new Rating();

        $like["Rating"]["video_id"] = $id;
        $like["Rating"]["user_id"] = Yii::$app->user->id;
        $like["Rating"]["positive"] = 0;

        $likes = $rating->find()->where(['video_id' => $id, 'user_id' => Yii::$app->user->id])->all();

        if (!empty($likes)) {
            $likes[0]->positive = 0;
            $likes[0]->save();
            return $this->redirect(['video/'.$id]);
        } else {
            $rating->load($like) && $rating->save();
            return $this->redirect(['video/'.$id]);
        }
    }

    /**
     * Finds Favorite model related to the video and logged in user and deletes it.
     * In case no model is found, it is created.
     * @return mixed
     */
    public function actionFavorite($id)
    {
        if (Yii::$app->user->getIsGuest()) {
            //not logged in
            return $this->redirect(['user/login']);
        }

        $model = new Favorite();
        $model["video_id"] = $id;
        $model["user_id"] = Yii::$app->user->id;

        $exists = Favorite::find()->where(['video_id' => $id, 'user_id' => Yii::$app->user->id])->all();

        if (empty($exists)) {
            $model->save();
            return $this->redirect(['video/'.$id]);
        } else {
            $delete = Favorite::findOne($exists[0]->id);
            $delete->delete();
            return $this->redirect(['video/'.$id]);
        }
    }

    /**
     * Finds the Video model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Video the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Video::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
