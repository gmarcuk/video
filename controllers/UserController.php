<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\Video;
use app\models\Favorite;
use app\models\Comment;
use app\models\Visit;
use app\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\LoginForm;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * Only available to logged in admin user.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->getIsGuest()) {
            //not logged in
            return $this->redirect(['user/login']);
        }

        if (!Yii::$app->user->identity->isAdmin()) {
            // is admin
            return $this->redirect(['video/index']);
        }

        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model and video models related to it. Along with video models, all visits and users models are passed into view.
     * Only available to logged in users.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (Yii::$app->user->getIsGuest()) {
            //not logged in
            return $this->redirect(['user/login']);
        }

        $videomodel = new Video();
        $videos = $videomodel->find()->where(['user_id' => $id])->all();

        // visits
        $visitmodel = new Visit();
        $visits = $visitmodel->find()->all();

        // users
        $usermodel = new User();
        $users = $usermodel->find()->all();

        return $this->render('view', [
            'model' => $this->findModel($id),
            'videos' => $videos,
            'users' => $users,
            'visits' => $visits,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page of user.
     * Only available to the logged in user of this model.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->getIsGuest()) {
            //not logged in
            return $this->redirect(['user/login']);
        }

        $model = $this->findModel($id);

        if (Yii::$app->user->identity->id != $model->user_id) {
            // is admin
            return $this->redirect(['index']);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the login page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->getIsGuest()) {
            //not logged in
            return $this->redirect(['user/login']);
        }

        $model = $this->findModel($id);

        if (Yii::$app->user->identity->id != $model->user_id) {
            // is admin
            return $this->redirect(['index']);
        }

        // remove favorites
        $favoritemodel = new Favorite();
        $favorites = $favoritemodel->find()->where(['user_id' => $id])->all();
        foreach($favorites as $favorite) {
            $favorite->delete();
        }

        // remove favorites
        $commentmodel = new Comment();
        $comments = $commentmodel->find()->where(['user_id' => $id])->all();
        foreach($comments as $comment) {
            $comment->delete();
        }

        // remove favorites
        $videomodel = new Video();
        $videos = $videomodel->find()->where(['user_id' => $id])->all();
        foreach($videos as $video) {
            $videofavorites = $favoritemodel->find()->where(['user_id' => $id])->all();
            $videocomments = $commentmodel->find()->where(['user_id' => $id])->all();
            foreach($videocomments as $comment) {
                $comment->delete();
            }
            foreach($videofavorites as $comment) {
                $comment->delete();
            }
            $video->delete();
        }

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Displays register page.
     * If user is logged in, page is redirected to "video/index" page.
     * @return string
     */
    public function actionRegister()
    {
        $user = new User();

        if (!Yii::$app->user->getIsGuest()) {
            //not logged in
            return $this->redirect(['video/index']);
        }

        if ($user->load(Yii::$app->request->post()) && $user->register($user->load(Yii::$app->request->post()))) {

//            \Yii::$app->mailer->compose('/mail/registration', ['user' => $user])
//                ->setFrom(['noreply@olv.example' => 'Registration'])
//                ->setTo($user->username)
//                ->setSubject('Registration | OLV')
//                ->send();

            return $this->goBack();
        } else {
            return $this->render('register', [
                'user' => $user,
            ]);
        }
    }

    /**
     * Displays Login page.
     * If user is logged in, page is redirected to "video/index" page.
     * @return string
     */
    public function actionLogin()
    {
        $login = new LoginForm();

        if (!Yii::$app->user->getIsGuest()) {
            //not logged in
            return $this->redirect(['video/index']);
        }

        if ($login->load(Yii::$app->request->post()) && $login->login()) {
            $session= Yii::$app->session;
            $session->open();
            return $this->goBack();
        }

        return $this->render('login', [
            'login' => $login,
        ]);
    }

    /**
     * Logout action.
     * Page is redirected to landing login.
     * @return string
     */
    public function actionLogout()
    {
        if (Yii::$app->user->getIsGuest()) {
            //not logged in
            return $this->redirect(['user/login']);
        }

        $session = Yii::$app->session;
        $session->destroy();
        Yii::$app->user->logout();
        return $this->redirect(['user/login']);
    }
}
