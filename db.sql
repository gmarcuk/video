-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: Jan 12, 2017 at 08:57 PM
-- Server version: 5.5.38
-- PHP Version: 5.6.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `video`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
`id` int(6) NOT NULL,
  `content` varchar(260) COLLATE utf8_czech_ci NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `video_id` int(6) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `content`, `time`, `user_id`, `video_id`) VALUES
(35, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in rep', '2017-01-12 18:55:54', 27, 39),
(36, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', '2017-01-12 18:56:04', 27, 39),
(37, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ', '2017-01-12 18:57:05', 28, 39);

-- --------------------------------------------------------

--
-- Table structure for table `favorites`
--

CREATE TABLE `favorites` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `video_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Dumping data for table `favorites`
--

INSERT INTO `favorites` (`id`, `user_id`, `video_id`) VALUES
(29, 27, 39),
(30, 28, 44),
(31, 27, 40),
(32, 27, 49),
(33, 28, 46);

-- --------------------------------------------------------

--
-- Table structure for table `rating`
--

CREATE TABLE `rating` (
`id` int(6) NOT NULL,
  `video_id` int(6) NOT NULL,
  `user_id` int(11) NOT NULL,
  `positive` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Dumping data for table `rating`
--

INSERT INTO `rating` (`id`, `video_id`, `user_id`, `positive`) VALUES
(28, 39, 27, 1),
(29, 39, 28, 0),
(30, 44, 28, 1),
(31, 40, 27, 1),
(32, 49, 27, 1),
(33, 46, 28, 1),
(34, 49, 28, 1),
(35, 55, 27, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
`user_id` int(6) NOT NULL,
  `username` varchar(30) COLLATE utf8_czech_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `firstname` varchar(30) COLLATE utf8_czech_ci NOT NULL,
  `lastname` varchar(30) COLLATE utf8_czech_ci NOT NULL,
  `student_id` varchar(30) COLLATE utf8_czech_ci NOT NULL,
  `auth_key` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `type` int(6) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `firstname`, `lastname`, `student_id`, `auth_key`, `type`) VALUES
(4, 'emilio@mail.com', '$2y$13$rpSHp9mEvVrASbd92MektOlCqnbaXXbHfWv..GW9ygk5e0gKXYBg.', 'Emilio', 'Herrera', '666666', 'qQPNSQFJTryxWsUBg5w3m-IXMN5fAzAg', 0),
(27, 'gmarcuk@gmail.com', '$2y$13$0TsTLblIDCwmyBhnUYSXsOae8YWjFI5Mjh2fgmxoM9SWZoQD97LHS', 'Georgy', 'Marchuk', 'abc123', 'GYEVbOJHvOGVTrJgWsIj4wwmFGJRDied', 1),
(28, 'test@test.com', '$2y$13$8VYDM7WzWzi8pgj8b7WXFe0rLmDwOJgtUSP.kycPNlY0RDInYHtcO', 'Firstname', 'Lastname', '123456789', 'mmukIPSkCj5_NEwo464ZF1nJwY9ABcbm', 0);

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
`id` int(6) NOT NULL,
  `title` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `url` varchar(300) COLLATE utf8_czech_ci NOT NULL,
  `labels` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `description` varchar(800) COLLATE utf8_czech_ci NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `img` varchar(300) COLLATE utf8_czech_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `title`, `url`, `labels`, `description`, `date`, `user_id`, `img`) VALUES
(39, 'Eminem - Lose Yourself [HD]', 'https://www.youtube.com/watch?v=_Yhyp-_hX2s', 'Music', 'feat. Eminem from the movie 8 MILE No copyright infringement intended. All contents belong to its rightful owners. This is for entertainment purposes only.', '2017-01-12 18:39:58', 27, 'https://i.ytimg.com/vi/_Yhyp-_hX2s/maxresdefault.jpg'),
(40, 'Eminem - When I&#39;m Gone', 'https://www.youtube.com/watch?v=1wYNFfgrXTI', 'Music', 'Music video by Eminem performing When I&#39;m Gone. (C) 2005 Aftermath Entertainment/Interscope Records', '2017-01-12 19:13:05', 28, 'https://i.ytimg.com/vi/1wYNFfgrXTI/maxresdefault.jpg'),
(41, 'Malgin shaken up after devastating hit from Zacha', 'https://www.youtube.com/watch?v=DWRarCd1IzQ', 'Sports', 'Florida Panthers forward Denis Malgin is forced to leave after getting a hard hit from Pavel Zacha of the Devils.', '2017-01-12 19:16:50', 28, 'https://i.ytimg.com/vi/DWRarCd1IzQ/maxresdefault.jpg'),
(42, 'Eminem - Till I Collapse [HD]', 'https://www.youtube.com/watch?v=ytQ5CYE1VZw', 'Music', 'feat. Hugh Jackman from the movie REAL STEEL No copyright infringement intended. All contents belong to its rightful owners. This is for entertainment purpos...', '2017-01-12 19:17:27', 27, 'https://i.ytimg.com/vi/ytQ5CYE1VZw/hqdefault.jpg'),
(43, 'Martin levels Petry and 2 fights break out', 'https://www.youtube.com/watch?v=1yLTAI9r24Y', 'Sports', 'After Martin crushed Petry along the boards, line brawl ensures with Martin fighting McCarron and Gauthier going at with Farnham.', '2017-01-12 19:18:19', 28, 'https://i.ytimg.com/vi/1yLTAI9r24Y/maxresdefault.jpg'),
(44, 'Gotta See It: Emelin launches Hyman into Price', 'https://www.youtube.com/watch?v=rVuL_6PkJ2g', 'Sports', 'Watch as Zach Hyman takes a hit from Alexei Emelin and crashes into Carey Price.', '2017-01-12 19:19:57', 28, 'https://i.ytimg.com/vi/rVuL_6PkJ2g/maxresdefault.jpg'),
(46, '10 Amazing Science Tricks Using Liquid!', 'https://www.youtube.com/watch?v=HQx5Be9g16U', 'Physics', 'Add me on Facebook (click LIKE on Facebook to add me) - http://www.facebook.com/brusspup Download the music in this video: Song #1: Abyss - iTunes: https://i...', '2017-01-12 19:20:59', 27, 'https://i.ytimg.com/vi/HQx5Be9g16U/maxresdefault.jpg'),
(47, '9 Awesome Science Tricks Using Static Electricity!', 'https://www.youtube.com/watch?v=ViZNgU-Yt-Y', 'Physics', 'Add me on Facebook. (click the LIKE button on Facebook to add me) http://www.facebook.com/brusspup Music in the video are songs I created. Song #1: Over Rain...', '2017-01-12 19:21:24', 28, 'https://i.ytimg.com/vi/ViZNgU-Yt-Y/maxresdefault.jpg'),
(48, '9 Awesome Science Tricks Using Static Electricity!', 'https://www.youtube.com/watch?v=ViZNgU-Yt-Y', 'Physics', 'Add me on Facebook. (click the LIKE button on Facebook to add me) http://www.facebook.com/brusspup Music in the video are songs I created. Song #1: Over Rain...', '2017-01-12 19:23:13', 28, 'https://i.ytimg.com/vi/ViZNgU-Yt-Y/maxresdefault.jpg'),
(49, 'How and Why We Read: Crash Course English Literature #1', 'https://www.youtube.com/watch?v=MSYw502dJNY', 'Literature', 'In which John Green kicks off the Crash Course Literature mini series with a reasonable set of questions. Why do we read? What&#39;s the point of reading critica...', '2017-01-12 19:24:13', 27, 'https://i.ytimg.com/vi/MSYw502dJNY/maxresdefault.jpg'),
(50, 'Database Lesson #1 of 8 - Introduction to Databases', 'https://www.youtube.com/watch?v=4Z9KEBexzcM', 'Math', 'Dr. Soper gives an introductory lecture on database technologies. Topics covered include the reasons for using a database, the components of a database syste...', '2017-01-12 19:26:18', 27, 'https://i.ytimg.com/vi/4Z9KEBexzcM/maxresdefault.jpg'),
(51, 'history of japan', 'https://www.youtube.com/watch?v=Mh5LY4Mz15o', 'Math', 'http://billwurtz.com patreon: http://patreon.com/billwurtz spotify: https://play.spotify.com/artist/78cT0dM5Ivm722EP2sgfDh itunes: http://itunes.apple.com/us...', '2017-01-12 19:27:11', 28, 'https://i.ytimg.com/vi/Mh5LY4Mz15o/maxresdefault.jpg'),
(52, 'Czech history in 10 minutes', 'https://www.youtube.com/watch?v=GWn2eLcdj0Y', 'History', 'Ceska historie české dějiny česká historie Ceske dejiny Great Moravia Czech history history of Bohemia history of Czechoslovakia history of Czech Republic Ts...', '2017-01-12 19:27:38', 28, 'https://i.ytimg.com/vi/GWn2eLcdj0Y/hqdefault.jpg'),
(53, 'Basic Computing Skills - Orientation', 'https://www.youtube.com/watch?v=DwsKeoXOa9I', 'IT', 'Worried your experience with computers won&#39;t be up to university standard? This video will help you get a grip on the basic computer skills needed for study....', '2017-01-12 19:28:28', 27, 'https://i.ytimg.com/vi/DwsKeoXOa9I/hqdefault.jpg'),
(54, 'Introduction to Biology-HD on Vimeo', 'https://vimeo.com/73171433', 'Biology', 'This is a new high definition (HD) dramatic video choreographed to powerful music that introduces the viewer/student to the Science of Biology. It is designed as…', '2017-01-12 19:31:54', 28, 'https://i.vimeocdn.com/filter/overlay?src0=https%3A%2F%2Fi.vimeocdn.com%2Fvideo%2F447201335_1280x720.jpg&src1=https%3A%2F%2Ff.vimeocdn.com%2Fimages_v6%2Fshare%2Fplay_icon_overlay.png'),
(55, 'Why is Math Different Now on Vimeo', 'https://vimeo.com/110807219', 'Math', 'Dr. Raj Shah explains why math is taught differently than it was in the past and helps address parents&#039; misconceptions about the "new math".  Dr.…', '2017-01-12 19:32:59', 27, 'https://i.vimeocdn.com/video/495192583_1280x720.jpg'),
(56, 'MP4 linked file', 'http://www.w3schools.com/html/mov_bbb.mp4', 'Other', 'MP4 video file linked.', '2017-01-12 19:35:13', 28, ''),
(57, 'MP4 linked file', 'http://www.quirksmode.org/html5/videos/big_buck_bunny.mp4', 'Math', 'Just another video MP4 file linked ', '2017-01-12 19:36:31', 28, '');

-- --------------------------------------------------------

--
-- Table structure for table `visited`
--

CREATE TABLE `visited` (
`id` int(11) NOT NULL,
  `video_id` int(11) NOT NULL,
  `user_id` int(6) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=227 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Dumping data for table `visited`
--

INSERT INTO `visited` (`id`, `video_id`, `user_id`, `time`) VALUES
(201, 39, 27, '2017-01-12 18:39:58'),
(202, 39, 27, '2017-01-12 18:54:07'),
(203, 39, 28, '2017-01-12 18:56:53'),
(204, 39, 27, '2017-01-12 19:10:19'),
(205, 40, 28, '2017-01-12 19:13:05'),
(206, 41, 28, '2017-01-12 19:16:50'),
(207, 42, 27, '2017-01-12 19:17:27'),
(208, 43, 28, '2017-01-12 19:18:19'),
(209, 45, 28, '2017-01-12 19:20:01'),
(210, 46, 27, '2017-01-12 19:21:00'),
(211, 47, 28, '2017-01-12 19:21:24'),
(212, 48, 28, '2017-01-12 19:23:14'),
(213, 50, 27, '2017-01-12 19:26:19'),
(214, 54, 28, '2017-01-12 19:31:54'),
(215, 55, 27, '2017-01-12 19:32:59'),
(216, 56, 28, '2017-01-12 19:35:13'),
(217, 57, 28, '2017-01-12 19:36:36'),
(218, 57, 27, '2017-01-12 19:36:47'),
(219, 56, 27, '2017-01-12 19:45:41'),
(220, 57, 28, '2017-01-12 19:53:13'),
(221, 44, 28, '2017-01-12 19:53:39'),
(222, 40, 27, '2017-01-12 19:53:47'),
(223, 49, 27, '2017-01-12 19:54:01'),
(224, 46, 28, '2017-01-12 19:54:27'),
(225, 49, 28, '2017-01-12 19:54:39'),
(226, 55, 27, '2017-01-12 19:54:53');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favorites`
--
ALTER TABLE `favorites`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rating`
--
ALTER TABLE `rating`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`user_id`), ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visited`
--
ALTER TABLE `visited`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
MODIFY `id` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `favorites`
--
ALTER TABLE `favorites`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `rating`
--
ALTER TABLE `rating`
MODIFY `id` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `user_id` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
MODIFY `id` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `visited`
--
ALTER TABLE `visited`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=227;