var fs = require('fs');

module.exports = function(filePath) {
    try
    {
        return fs.statSync(filePath).isDirectory();
    }
    catch (err)
    {
        return false;
    }
}