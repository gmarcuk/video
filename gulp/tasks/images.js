var gulp        = require('gulp');
var gutil       = require('gulp-util');
var changed     = require('gulp-changed');
var rename      = require('gulp-rename');
var filter      = require('gulp-filter');
var imagemin    = require('gulp-imagemin');
var raster      = require('gulp-raster');
var pngquant    = require('imagemin-pngquant');
var config      = require('../config').images;

var debug = require('gulp-debug');

gulp.task('svg2png', function() {
    return gulp.src(config.rasterSrc)
        .pipe(changed(config.dest))
        .pipe(raster())
        .pipe(rename({extname: '.png'}))
        .pipe(gulp.dest(config.rasterDest));
});

gulp.task('images', ['svg2png'], function() {

    if ('rawSrc' in config) {
        gutil.log(gutil.colors.yellow('Složka img-raw není již podporovaná. Spusťe gulp upgrade.'));
    }

    var optimizationFilter = filter(function(file) {
        return /[^@]\.[^\.]*$/.test(file.path);
    }, {
        restore: true
    });

    var renameFilter = filter(function(file) {
        return /@\.[^\.]*$/.test(file.path);
    }, {
        restore: true
    });

    return gulp.src(config.src)
        .pipe(changed(config.dest))
        .pipe(optimizationFilter)
        .pipe(imagemin({
            progressive: true,
            interlaced: true,
            optimizationLevel: 5,
            use: [
                pngquant({
                    quality: '70-90',
                    speed: 4
                })
            ]
        }))
        .pipe(optimizationFilter.restore)
        .pipe(renameFilter)
        .pipe(rename(function(path) {
            path.basename = path.basename.replace(/@$/, '');
        }))
        .pipe(renameFilter.restore)
        .pipe(gulp.dest(config.dest));
});