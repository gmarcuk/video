var gulp     = require('gulp');
var gutil       = require('gulp-util');
var rename      = require('gulp-rename');
var debug       = require('gulp-debug');
var fs          = require('fs');
var path        = require('path');
var mergeStream = require('merge-stream');
var runSequence = require('run-sequence');
var del      = require('del');
var config      = require('../config');

var getFolders  = require('../util/getFolders.js');
var getFiles    = require('../util/getFiles.js');
var folderExists = require('../util/folderExists.js');

var src = './src';

gulp.task('upgrade:image-raw', function(callback) {
    var tasks = [];

    var targets = getFolders('./src');
    tasks = targets.map(function(target) {
        var folder = path.join(src, target, 'img-raw');
        var imageFolder = path.join(src, target, 'img');

        if (folderExists(folder)) {
            return gulp.src(folder + '/**/*')
                .pipe(rename(function(path) {
                    // skip folders
                    if (path.extname) {
                        path.basename += '@';
                    }
                }))
                .pipe(gulp.dest(imageFolder))
                .on('end', function() {
                    del(folder);
                    gutil.log('[upgrade:image-raw]', gutil.colors.yellow('Moved'), gutil.colors.blue(folder));
                });
        }

        return null;
    }).filter(function(task) {
        return task;
    });

    if (tasks.length) {
        return mergeStream(tasks);
    } else {
        gutil.log('[upgrade:image-raw]', gutil.colors.green('Nothing to upgrade'));
        callback();
    }
});

gulp.task('upgrade:image-raw:config', function(callback) {
    if (!('rawSrc' in config.images)) {
        gutil.log('[upgrade:image-raw:config]', gutil.colors.green('Nothing to upgrade'));
        callback();
    } else {
        var file = './gulp/config.js';
        fs.readFile(file, 'utf8', function(err, data) {
            data = data.replace(/\s*rawSrc[^\n]*/, '');
            fs.writeFile(file, data);
            gutil.log('[upgrade:image-raw:config]', gutil.colors.yellow('Config updated'));
            callback();
        });
    }
});

gulp.task('upgrade', function(callback) {
     runSequence('upgrade:image-raw', 'upgrade:image-raw:config', callback);
});