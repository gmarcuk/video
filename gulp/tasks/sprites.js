var gulp        = require('gulp');
var spritesmith = require('gulp.spritesmith');
var rename      = require('gulp-rename');
var path        = require('path');
var fs          = require('fs');
var mergeStream = require('merge-stream');
var getFiles    = require('../util/getFiles');
var getFolders  = require('../util/getFolders');
var folderExists = require('../util/folderExists');
var fileExists  = require('../util/fileExists');
var config      = require('../config').sprites;

function cleanupImages(sourceFolder, optimization) {
    if (folderExists(sourceFolder)) {
        var images = getFiles(sourceFolder);
        images.forEach(function(file) {
            var folder = path.join(config.src, path.basename(file.replace('@2x',''), '.png'));
            var image = path.join(sourceFolder, file);

            if (optimization) {
                if (!folderExists(folder) && !folderExists(folder + '@')) {
                    fs.unlinkSync(image);
                }
            } else {
                if (!folderExists(folder)) {
                    fs.unlinkSync(image);
                }
            }

        });
    }
}

gulp.task('sprites:clean', function(callback) {

    cleanupImages(config.img, false);
    cleanupImages(config.dest, true);

    // css cleanup
    if (folderExists(config.css)) {
        var styles = getFiles(config.css, 'sprite');
        styles.forEach(function(file) {
            var name = path.basename(file, '.styl').replace('sprite-', '');
            var folder = path.join(config.src, name);
            var style = path.join(config.css, file);

            if (!folderExists(folder) && !folderExists(folder + '@')) {
                fs.unlinkSync(style);
            }
        });
    }

    callback();
});

gulp.task('sprites', ['sprites:clean'], function(callback) {
    var folders = getFolders(config.src);
    var tasks = folders.filter(function(folder) {
        // optimization flag
        var folderName = folder.replace(/@$/, '');

        var source = path.join(config.src, folder);
        var image = path.join(config.img, folder + '.png');
        var style = path.join(config.css, 'sprite-' + folderName + '.styl');

        return !fs.existsSync(image) || !fs.existsSync(style) || fs.statSync(source).mtime > fs.statSync(image).mtime;
    }).map(function(folder) {
        // optimization flag
        var folderName = folder.replace(/@$/, '');

        var retinaFiles = getFiles(path.join(config.src, folder), '@2x');

        var spriteOptions = {
            imgName: folder + '.png',
            imgPath: 'sprites/' + folderName + '.png',
            algorithm: 'binary-tree',
            cssName: 'sprite-' + folderName + '.styl',
            // cssVarMap: function (sprite) {
            //     // sprite.name = 'sprite-' + folder + '-' + sprite.name;
            // },
            cssTemplate: config.template,
            cssOpts: {
                'functions': true,
                'name': folderName,
                'retina': false
            }
        };

        if (retinaFiles.length) {
            spriteOptions.retinaSrcFilter = path.join(config.src, folder, '/*@2x.png');
            spriteOptions.retinaImgName = folderName + '@2x' + (folderName !== folder ? '@' : '') + '.png';
            spriteOptions.retinaImgPath = 'sprites/' + folderName + '@2x.png';
            spriteOptions.cssOpts.retina = true;
        }

        var spriteData = gulp.src(path.join(config.src, folder, '/*.png'))
            .pipe(spritesmith(spriteOptions));

        spriteData.img.pipe(gulp.dest(config.img));
        spriteData.css.pipe(gulp.dest(config.css));

        return spriteData;
    });

    if (tasks.length) {
        return mergeStream(tasks);
    } else {
        callback();
    }
});