var gulp = require('gulp');
var runSequence = require('run-sequence');
var options = require('../config').options;

gulp.task('build', function(callback) {
    runSequence(['iconfont', 'sprites'], ['fonts', 'images'], ['styles', 'scripts'], callback);
});