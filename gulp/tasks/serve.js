var gulp    = require('gulp');
var runSequence = require('run-sequence');
var config  = require('../config');

gulp.task('serve', function(callback) {
    runSequence('watch', 'browserSync', callback);
});