var pkg = require('../package.json');
var argv = require('minimist')(process.argv.slice(2), {
    alias: {
        p: 'production',
        t: 'target'
    }
});
var os = require('os');

var options = {
    production: argv.production,
    target: argv.target || 'frontend',
    platform: os.platform()
};

var dest = 'web/assets/custom/';
var src = 'src/' + options.target;
var templates = 'gulp/templates';

module.exports = {
    options: options,
    browserSync: {
        proxy: pkg.name + ".yii",
        files: [
            dest + '/**',
            '!' + dest + '/**.map'
        ]
    },
    styles: {
        src: [src + '/css/*.styl', '!' + src + '/css/imports.styl'],
        dest: dest + '/css',
        watch: [src + '/css/**/*.styl', '!' + src + '/css/_exports/sprite*.styl'],
        imgRoot: '../img/',
        paths: dest + '/img/'
    },
    images: {
        src: src + '/img/**/*',
        watch: [src + '/img/**/*'],
        dest: dest + '/img',
        rasterSrc: src + '/img/**/*.svg',
        rasterDest: src + '/img'
    },
    iconfont: {
        name: 'icons',
        src: src + '/iconfont/*.svg',
        dest: dest + '/fonts/icons',
        template: templates + '/iconfont.lodash',
        path: '../fonts/icons/',
        css: src + '/css/_exports'
    },
    fonts: {
        src: src + '/fonts/**/*',
        dest: dest + '/fonts'
    },
    sprites: {
        src: src + '/sprites',
        watch: src + '/sprites/**/*.png',
        css: src + '/css/_exports',
        img: src + '/img/sprites',
        dest: dest + '/img/sprites',
        template: templates + '/sprite-template.handlebars'
    },
    scripts: {
        src: [
            src + '/js/plugins/**/*.js',
            src + '/js/global.js',
            src + '/js/modules/**/*.js',
            src + '/js/app.js'
        ],
        dest: dest + '/js',
        vendorDest: dest + '/js/vendor',
        watch: src + '/js/**/*.js',
        copy: [
            src + '/js/vendor/**/*.js'
        ]
    }
};