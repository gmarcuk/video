<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $firstname
 * @property string $lastname
 * @property string $student_id
 * @property string $auth_key
 * @property integer $type
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password', 'firstname', 'lastname', 'student_id', 'type'], 'required'],
            [['username'], 'unique'],
            [['username'], 'email'],
            [['student_id'], 'unique'],
            [['type'], 'integer'],
            [['username', 'firstname', 'lastname', 'student_id'], 'string', 'max' => 30],
            [['password', 'auth_key'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'ID',
            'username' => 'Email',
            'password' => 'Password',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'student_id' => 'Student ID',
            'auth_key' => 'Auth Key',
            'type' => 'Type',
        ];
    }

    /**
     * Saves new user model to database.
     * @return User|null
     */
    public function register()
    {
        $this->type = 0;
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->username = $this->username;
        $user->setPassword($this->password);
        $user->firstname = $this->firstname;
        $user->lastname = $this->lastname;
        $user->student_id = $this->student_id;
        $user->type = $this->type;

        return $user->save() ? $user : null;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        $dbUser = User::find()->where(["user_id" => $id])->one();
        if (!count($dbUser)) {
            return null;
        }
        return new static($dbUser);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        $dbUser = User::find()->where(["username" => $username])->one();

        if (!count($dbUser)) {
            return null;
        }
        return new static($dbUser);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->user_id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->firstname.' '.$this->lastname.' ('.$this->username.')';
    }


    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * @inheritdoc
     */
    public function setPassword($password)
    {
        return $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->auth_key = \Yii::$app->security->generateRandomString();
            }
            return true;
        }
        return false;
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $password attribute currently being validated
     * @param string $hash hash generated from the password at creation of the model
     * @return boolean whether password and hash fit
     */
    public function validatePassword($password, $hash)
    {
        if (Yii::$app->getSecurity()->validatePassword($password, $hash)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validateLogin()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*30 : 0);
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }

    /**
     * Validates login credential
     *
     * @return User|null
     */
    public function validateLogin()
    {
        if (User::findByUsername($this->username)) {
            print_r(Yii::$app->security->generatePasswordHash($this->password));
            return false;
        }

        return false;
    }

    /**
     * Check whether user is logged in
     *
     * @return boolean
     */
    public function isLoggedIn()
    {
        if (!Yii::$app->user->getIsGuest()) {
            return true;
        }
        return false;
    }

    /**
     * Check whether logged in user is admin
     *
     * @return boolean
     */
    public function isAdmin()
    {
        if ($this->type) {
            return true;
        }
        return false;
    }
}
