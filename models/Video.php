<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "videos".
 *
 * @property integer $id
 * @property string $title
 * @property string $url
 * @property string $module
 * @property string $description
 * @property string $date
 * @property integer $user_id
 */
class Video extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'videos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'url', 'labels', 'description'], 'required'],
            [['date'], 'safe'],
            [['user_id'], 'integer'],
            [['title'], 'string', 'max' => 64],
            [['labels'], 'in', 'range' => ['Math' => 'Math', 'Biology' => 'Biology', 'IT' => 'IT', 'History' => 'History', 'Database' => 'Database', 'Literature' => 'Literature', 'Physics' => 'Physics', 'Music' => 'Music', 'Sports' => 'Sports', 'Other' => 'Other']],
            [['url'], 'string', 'max' => 300],
            [['url'], 'url', 'defaultScheme' => 'http'],
            [['description'], 'string', 'max' => 800],
            [['img'], 'string', 'max' => 300]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'url' => 'Url',
            'labels' => 'Labels',
            'description' => 'Description',
            'date' => 'Date',
            'user_id' => 'User ID',
            'img' => 'Image',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                if (Yii::$app->user->id) {
                    $this->user_id = Yii::$app->user->id;
                    return true;
                }
                return false;
            } else {
                if ($this->user_id == Yii::$app->user->id) {
                    return true;
                }
                return false;
            }
        }
        return false;
    }
}
