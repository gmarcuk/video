<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comments".
 *
 * @property integer $id
 * @property string $content
 * @property string $time
 * @property integer $user_id
 * @property integer $video_id
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content'], 'required'],
            [['time'], 'safe'],
            [['user_id', 'video_id'], 'integer'],
            [['content'], 'string', 'max' => 260],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content' => 'Content',
            'time' => 'Time',
            'user_id' => 'User ID',
            'video_id' => 'Video ID',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                if (Yii::$app->user->id) {
                    $this->user_id = Yii::$app->user->id;
                    return true;
                }
                return false;
            } else {
                if ($this->user_id == Yii::$app->user->id) {
                    return true;
                }
                return false;
            }
        }
        return false;
    }
}
