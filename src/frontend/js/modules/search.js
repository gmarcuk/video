(function () {
    $(document).on('click', '[data-search]', function (event) {
        event.stopPropagation();
        $('body').toggleClass('show-search');
    });

    $(document).on('click', function (event) {
        $('body').removeClass('show-search');
    });

    $(document).on('click', '.search', function (event) {
        event.stopPropagation();
    });
}());