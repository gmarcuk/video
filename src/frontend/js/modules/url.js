(function () {
    $(document).on('change', '[data-url]',function (e) {

        var query = 'select * from html where url="' + $(this).val() + '" and xpath="*"';
        var url = 'https://query.yahooapis.com/v1/public/yql?q=' + encodeURIComponent(query);

        $.ajax({
            url: url,
            success: function (responseHtml) {
                $('#video-title').val($(responseHtml).find('meta[name="title"]').attr('content') || $(responseHtml).find('title').text() || $(responseHtml).find('meta[property="og:title"]').attr('content') || $(responseHtml).find('title').text());
                $('#video-description').val($(responseHtml).find('meta[name="description"]').attr('content') || $(responseHtml).find('meta[property="og:description"]').attr('content'));
                if ($(responseHtml).find('meta[property="og:image"]').length) {
                    $('#img-preview').css('display', 'inline-block');
                    $('#img-preview img').attr('src', $(responseHtml).find('meta[property="og:image"]').attr('content'));
                    $('#video-img').val($(responseHtml).find('meta[property="og:image"]').attr('content'));
                } else {
                    $('#img-preview').hide();
                }
            }
        });
    });

    $(document).on('change', '#video-img', function () {
        $('#img-preview img').attr('src', $(this).val());
    });
}());