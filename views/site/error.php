<?php

/* @var $this yii\web\View */
use yii\helpers\Html;

$this->title = 'Error occured';
?>
<main rel="main" class="main">
    <h1><?= $this->title; ?> - <?= nl2br(Html::encode($message)) ?></h1>
</main>
