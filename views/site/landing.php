<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Online Learning Video Repository';
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" sizes="57x57" href="assets/custom/fav/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="assets/custom/fav/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="assets/custom/fav/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="assets/custom/fav/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="assets/custom/fav/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="assets/custom/fav/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="assets/custom/fav/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="assets/custom/fav/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="assets/custom/fav/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="assets/custom/fav/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="assets/custom/fav/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="assets/custom/fav/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="assets/custom/fav/favicon-16x16.png">
        <link rel="manifest" href="assets/custom/fav/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="assets/custom/fav/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">

        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <link rel="stylesheet" href="/assets/custom/css/main.css"/>
    </head>
    <body>
        <?php $this->beginBody() ?>

            <main rel="main" class="main main-landing">
                <div class="landing">
                    <div class="landing__holder">
                        <div class="landing__valign">
                            <img src="/assets/custom/img/logo-olv.svg" alt="OLV"/>
                            <h1>Welcome to <br/> <strong>Online Learning Video Repository</strong></h1>
                            <p>Proceed to <?= Html::a('login', ['user/login']) ?>. In case you are not registered user, you can <?= Html::a('create your account', ['user/register']) ?>.</p>
                            <?= Html::a('Login', ['user/login'], ['class' => 'btn btn--blue']) ?>
                            <?= Html::a('Register', ['user/register'], ['class' => 'btn btn--green']) ?>
                        </div>
                    </div>
                </div>
            </main>

        <?php $this->endBody() ?>

        <script src="/assets/custom/js/app.js"></script>
    </body>
</html>
<?php $this->endPage() ?>