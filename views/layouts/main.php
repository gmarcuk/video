<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" sizes="57x57" href="assets/custom/fav/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="assets/custom/fav/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/custom/fav/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/custom/fav/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/custom/fav/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="assets/custom/fav/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="assets/custom/fav/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="assets/custom/fav/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/custom/fav/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="assets/custom/fav/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/custom/fav/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/custom/fav/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/custom/fav/favicon-16x16.png">
    <link rel="manifest" href="assets/custom/fav/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="assets/custom/fav/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <?= Html::csrfMetaTags() ?>
    <title><?php if(Html::encode($this->title) != 'Online Learning Video Repository'): ?><?= Html::encode($this->title).' | Online Learning Video Repository' ?><?php else: ?><?= Html::encode($this->title)?><?php endif ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

    <?= $this->render('//snippets/header') ?>

    <?= $this->render('//snippets/search') ?>

    <?= $content ?>

    <?= $this->render('//snippets/footer') ?>

<?php $this->endBody() ?>

<script src="/assets/custom/js/app.js"></script>
</body>
</html>
<?php $this->endPage() ?>
