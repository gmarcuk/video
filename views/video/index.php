<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\VideoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

if (isset($title)) {
    $this->title = $title;
} else {
    $this->title = "Videos";
}
?>
<div rel="main" class="main main-videos">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php if(isset($dashboard) && $dashboard): ?>
        <section class="section section--dashboard">
            <div class="section__inner">
                <?php echo $this->render('../snippets/dashboard', array('dashboard' => $dashboard)); ?>
            </div>
        </section>
    <?php endif ?>
    <section class="section">
        <div class="section__inner">
            <?php echo $this->render('../snippets/list-videos', array('videos' => $videos, 'users' => $users,'visits' => $visits)); ?>
        </div>
    </section>
</div>