<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Video */

$this->title = 'Create Video';
?>
<div rel="main" class="main main-form">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="section">
        <div class="section__inner">
            <?= $this->render('_form', [
                'model' => $model,
                'labels' => $labels
            ]) ?>
        </div>
    </div>
</div>
