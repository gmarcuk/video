<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Video */

$this->title = $model->title;
?>
<div rel="main" class="main main-video">
    <section class="section section--video">
        <div class="section__inner">
            <?php
                $urlParts = parse_url($model->url);
                if (isset($urlParts['query'])) {
                    $urlQuery = explode("=", $urlParts['query']);
                }

                if (preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $model->url, $youtube, PREG_OFFSET_CAPTURE)) {
                    echo '<div class="iframe"><iframe width="560" height="315" src="https://www.youtube.com/embed/'.$youtube[0][0].'" frameborder="0" allowfullscreen></iframe></div>';
                } else if ((int) substr(parse_url($model->url, PHP_URL_PATH), 1)) {
                    if (strpos($urlParts['host'], 'vimeo') !== false) {
                        echo '<div class="iframe"><iframe width="560" height="315" src="https://player.vimeo.com/video/'.(int) substr(parse_url($model->url, PHP_URL_PATH), 1).'" frameborder="0" allowfullscreen></iframe></div>';
                    } else {
                        echo '<div class="iframe"><iframe width="560" height="315" src="http://embed.'.str_replace('www.','',$urlParts['host']).'/?id='.(int) substr(parse_url($model->url, PHP_URL_PATH), 1).'" frameborder="0" allowfullscreen></iframe></div>';
                    }
                } else if (isset($urlQuery) && count($urlQuery) == 2) {
                    if ($urlQuery[0] == 'v' || $urlQuery[0] == 'viewkey') {
                        echo '<div class="iframe"><iframe width="560" height="315" name="custom-iframe" src="http://'.$urlParts['host'].'/embed/'.$urlQuery[1].'" frameborder="0" allowfullscreen scrolling="no"></iframe></div>';
                    } else {
                        echo '<h2>Linked url is not a video file.</h2>';
                    }
                } else {
                    // find content/type
                    if (strpos($headers[2], 'video') || strpos($headers[3], 'video')) {
                        echo '<video controls src="'.$model->url.'"></video>';
                    } else {
                        echo '<h2>Linked url is not a video file.</h2>';
                    }
                }
            ?>
        </div>
    </section>
    <section class="section section--description">
        <div class="section__inner">
            <?php
                $labels = explode(',', $model->labels);
                $colors = ['blue', 'orange', 'red', 'green', 'purple'];

                foreach($labels as $label) {
                    echo '<a href="/video/label/'.strtolower($label).'" class="labels labels__'.$colors[rand(0, 4)].'" style="opacity:'.(rand(8, 10)/10).'">'.$label.'</a>';
                }
            ?>
            <br/>
            <h1><?= Html::encode($this->title) ?></h1>
            <p class="grey"><a href="<?= $model->url; ?>" class="grey"><?= $model->url; ?></a></p>
            <div class="like">
                <a href="/video/like/<?= $model->id ?>" class="like__btn like__btn--like"><span class="icon icon-thumb-up"></span>&nbsp;<?= $rating->positive ?></a>
                <a href="/video/dislike/<?= $model->id ?>" class="like__btn like__btn--dislike"><span class="icon icon-thumb-down"></span>&nbsp;<?= $rating->negative ?></a>
                <?php if($rating->positive && $rating->negative): ?>
                    <div class="like__progress"><div style="width:<?= $rating->positive/($rating->positive+$rating->negative)*100-1 ?>%" data-note="<?= $rating->positive ?>&nbsp;likes"></div><div style="width:<?= $rating->negative/($rating->positive+$rating->negative)*100-1 ?>%" data-note="<?= $rating->negative ?>&nbsp;dislikes"></div></div>
                <?php endif ?>
            </div>
            <p><?= $model->description; ?></p>
            <p><strong>Time posted:</strong> <?= \Yii::$app->formatter->asDatetime($model->date, "php:d/m/Y H:i"); ?></p>
            <p class="views"><strong><?= $visits; ?></strong>&nbsp;views</p>
            <p><strong>Posted by: </strong><a href="/user/<?= $user->user_id ?>"><?= $user->getName(); ?></a></p>
            <?php if(!$favorite):?>
                <a href="/video/favorite/<?= $model->id ?>" class="btn btn--blue btn--small"><span class="icon icon-star icon--left"></span>Favorite</a>
            <?php else: ?>
                <a href="/video/favorite/<?= $model->id ?>" class="btn btn--orange btn--small"><span class="icon icon-star icon--left"></span>Unfavorite</a>
            <?php endif ?>
            <?php if(Yii::$app->user->identity->isAdmin() || Yii::$app->user->identity->id == $model->user_id): ?>
                <div class="admin-panel">
                    <a href="/video/update/<?= $model->id ?>" class="btn btn--small btn--orange"><span class="icon icon-listing-option icon--left"></span>Edit</a>
                    <a href="/video/delete/<?= $model->id ?>" class="btn btn--small btn--red"><span class="icon icon-trash icon--left"></span>Delete</a>
                </div>
            <?php endif ?>
        </div>
    </section>
    <section class="section section--comments">
        <div class="section__inner">
            <h2>Comments</h2>
            <?php echo $this->render('../comment/create', array('video' => $model, 'model' => $commentModel)); ?>
            <?php echo $this->render('../comment/video', array('video' => $model, 'model' => $commentModel, 'comments' => $comments, 'users' => $users)); ?>
        </div>
    </section>
</div>
