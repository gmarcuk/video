<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Video */

$this->title = 'Update Video: ' . $model->title;
?>
<main class="main main-form">
    <h1><?= Html::encode($this->title) ?></h1>

    <section class="section">
        <div class="section__inner">
            <?= $this->render('_form', [
                'model' => $model,
                'labels' => $labels,
            ]) ?>
        </div>
    </section>
</main>