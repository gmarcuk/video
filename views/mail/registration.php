<html>
    <head>
        <meta charset="UTF-8">
    </head>
    <body>
    <div style="text-align: center">
        <table width="600" style="color:#000;margin:auto;table-layout:fixed;text-align:left;width:600px;border:1px solid #aaa;border-top:3px solid #002538;border-bottom:none;padding:0 20px 20px 20px">
            <tbody>
            <tr>
                <td>
                    <p style="color:#002538;font-size:24px;text-align:right;font-family:sans-serif;line-height:1;margin:25px 0 5px;width:300px;float:right;display:inline-block;width:300px;vertical-align:middle">User registration</p>
                </td>
            </tr>
            </tbody>
        </table>
        <table width="600" style="color:#000;margin:auto;table-layout:fixed;text-align:left;width:600px;border:1px solid #aaa;border-top:none;padding:0 20px 20px 20px">

            <tbody>
            <tr>
                <td style="width:100%;font-family:sans-serif;line-height:1.4;font-size:15px;display:inline-block;font-weight:100;color:#000">
                    Hi,<br>
                    <br>
                    you have registered in our app with this email address (<?= $user->username; ?>). Your password is <strong><?= $user->password; ?></strong>.
                    <br/>
                    We do not store it and we do not allow password reset. Please keep it safe, or you will lose access to your account.
                    <br><br/>
                    Bye Bye! <br><br/>
                    OLV <br/>
                    <span style="color:aaa">Video Management System</span>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    </div>
    </body>
</html>