<?php

/* @var $this yii\web\View */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Login';
?>
<main rel="main" class="main main-form">
    <h1><?= $this->title; ?></h1>
    <div class="section">
        <div class="section__inner">
            <div class="form form--thin">
                <p>Please fill out the following fields to login:</p>

                <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($login, 'username')->textInput(['autofocus' => true]) ?>

                <?= $form->field($login, 'password')->passwordInput() ?>

                <?= $form->field($login, 'rememberMe')->checkbox() ?>

                <div class="form__control">
                    <?= Html::submitButton('Login<span class="icon icon-right-chevron icon--right"></span>', ['class' => 'btn btn--blue', 'name' => 'login-button']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</main>
