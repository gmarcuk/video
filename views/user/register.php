<?php

/* @var $this yii\web\View */
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Register User';
?>
<main rel="main" class="main main-form">
    <h1><?= $this->title; ?></h1>
    <div class="section">
        <div class="section__inner">
            <div class="form">
                <p>Please fill out the following fields to register:</p>
                <?php $form = ActiveForm::begin(); ?>
                <?= $form->field($user, 'username')->textInput(['autofocus' => true]) ?>
                <?= $form->field($user, 'password')->passwordInput() ?>
                <?= $form->field($user, 'firstname')->textInput() ?>
                <?= $form->field($user, 'lastname')->textInput() ?>
                <?= $form->field($user, 'student_id')->textInput() ?>
                <div class="form__control">
                    <?= Html::submitButton('Register<span class="icon icon-right-chevron icon--right"></span>', ['class' => 'btn btn--blue', 'name' => 'register-button']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</main>
