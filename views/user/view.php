<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->getName();
?>
<main class="main main-table">
    <h1><?= Html::encode($this->title) ?></h1>
    <section class="section section--thin">
        <div class="section__inner">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'username',
                    'firstname',
                    'lastname',
                    'student_id',
                ],
            ]) ?>
            <?php if(Yii::$app->user->identity->id == $model->user_id): ?>
                <div class="center">

                    <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn--small btn--orange']) ?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn--small btn--red',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
            <?php endif ?>
        </div>
    </section>
    <section class="section">
        <div class="section__inner">
            <?php echo $this->render('../snippets/list-videos', array('videos' => $videos, 'users' => $users,'visits' => $visits)); ?>
        </div>
    </section>
</main>