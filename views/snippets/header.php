<header class="header">
    <div class="header__holder">
        <a href="/" class="header__logo"><img src="/assets/custom/img/logo-olv.svg" alt="OLV"/></a>
        <div class="burger">
            <span></span>
        </div>
        <?php if(Yii::$app->user->isGuest): ?>
            <a href="/user/login" class="header__login">Login</a>
            <a href="/user/register" class="header__login">Register</a>
        <?php else: ?>
            <a href="/user/logout" class="header__login">Logout (<?= Yii::$app->user->identity->firstname ?>) <span class="icon icon-logout icon--right"></span></a>
            <a href="/video/create" class="header__add btn btn--blue btn--small">Add Video<span class="icon icon-plus icon--right"></span></a>
            <span  data-search class="header__add btn btn--green btn--small">Search<span class="icon icon-magnifying-glass icon--right"></span></span>
            <nav class="nav">
                <ul>
                    <li><a href="/video/my">My videos</a></li>
                    <li><a href="/video/favorites">Favorite</a></li>
                    <li><a href="/user/<?= Yii::$app->user->identity->id ?>">Profile</a></li>
                    <?php if(Yii::$app->user->identity->isAdmin()): ?>
                        <li><a href="/user">Users</a></li>
                    <?php endif ?>
                </ul>
            </nav>
        <?php endif ?>
    </div>
</header>

<nav class="nav nav-mobile">
    <div class="nav-mobile__holder">
        <ul>
            <?php if(Yii::$app->user->isGuest): ?>
                <li><a href="/user/login">Login</a></li>
                <li><a href="/user/register">Register</a></li>
            <?php else: ?>
                <?php if(Yii::$app->user->identity->isAdmin()): ?>
                    <li><a href="/user">Users</a></li>
                <?php endif ?>
                <li><a href="/user/<?= Yii::$app->user->identity->id ?>">Profile</a></li>
                <li><a href="/video/my">My videos</a></li>
                <li><a href="/video/favorites">Favorite</a></li>
                <li><a href="/user/logout">Logout (<?= Yii::$app->user->identity->firstname ?>) <span class="icon icon-logout icon--right"></span></a></li>
            <?php endif ?>
        </ul>
    </div>
</nav>