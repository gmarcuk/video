<ul class="dashboard">
    <li class="dashboard__item dashboard__item--red">
        <a href="/video/my">
            <h2><?= $dashboard->videos ?> <small>videos created</small></h2>
        </a>
    </li>
    <li class="dashboard__item dashboard__item--blue">
        <h2><?= $dashboard->comments ?> <small>comments written</small></h2>
    </li>
    <li class="dashboard__item dashboard__item--green">
        <a href="/video/favorites">
            <h2><?= $dashboard->favorited ?> <small>favorited videos</small></h2>
        </a>
    </li>
    <li class="dashboard__item dashboard__item--orange">
        <h2><?= $dashboard->rated ?> <small>videos rated</small></h2>
    </li>
</ul>
<div class="dashboard-labels">
    <a class="labels labels__blue" href="/video/label/math">Math</a>
    <a class="labels labels__orange" href="/video/label/biology">Biology</a>
    <a class="labels labels__red" href="/video/label/it">IT</a>
    <a class="labels labels__green" href="/video/label/history">History</a>
    <a class="labels labels__blue" href="/video/label/database">Database</a>
    <a class="labels labels__green" href="/video/label/literature">Literature</a>
    <a class="labels labels__orange" href="/video/label/physics">Physics</a>
    <a class="labels labels__red" href="/video/label/music">Music</a>
    <a class="labels labels__green" href="/video/label/sports">Sports</a>
    <a class="labels labels__blue" href="/video/label/other">Other</a>
</div>