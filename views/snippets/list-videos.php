<?php if(count($videos) != 0): ?>
    <ul class="list-videos">
        <?php foreach($videos as $video): ?>
            <?php
                // user of video
                foreach ($users as $user) {
                    if ($user->user_id == $video->user_id) {
                        $video_user = $user;
                        break;
                    }
                }
            ?>

            <?php
                // visits of video
                $video_visits = 0;
                foreach ($visits as $visit) {
                    if ($visit->video_id == $video->id) {
                        $video_visits++;
                    }
                }
            ?>
            <li>
                <a href="/video/<?= $video->id ?>">
                    <div class="list-videos__img">
                            <?php
                                $siteName = $urlQuery = explode(".", str_replace("www.", "", $urlParts = parse_url($video->url)['host']));
                                $siteName = array_slice($siteName, 0, -1);
                                $siteName = ucfirst(implode($siteName));
                                echo '<div class="list-videos__type">'.$siteName.'</div>';
                                if ($video->img !== null && $video->img !== '') {
                                    echo '<img src="' . $video->img .'" al>';
                                }
                            ?>
                    </div>
                    <p class="list-videos__url"><?= $video->url ?></p>
                    <h2><?= $video->title ?></h2>
                    <p class="list-videos__desc"><?= substr($video->description,0,120) ?>...</p>
                    <div class="list-videos__module">
                        <?php
                        $labels = explode(',', $video->labels);
                        $colors = ['blue', 'orange', 'red', 'green', 'purple'];

                        foreach($labels as $label) {
                            echo '<span class="labels labels__'.$colors[rand(0, 4)].'" style="opacity:'.(rand(8, 10)/10).'">'.$label.'</span>';
                        }
                        ?>
                    </div>
                    <div class="list-videos__user"><?= $video_user->getName(); ?></div>
                    <div class="list-videos__date"><?= \Yii::$app->formatter->asDatetime($video->date, "php:d/m/Y") ?></div>
                    <div class="list-videos__visits"><?= $video_visits ?> views</div>
                </a>
                <?php if(Yii::$app->user->identity->isAdmin() || Yii::$app->user->identity->id == $video->user_id): ?>
                    <div class="list-videos__admin">
                        <a href="/video/update/<?= $video->id ?>" class="btn btn--small btn--orange"><span class="icon icon-listing-option icon--left"></span>Edit</a>
                        <a href="/video/delete/<?= $video->id ?>" class="btn btn--small btn--red"><span class="icon icon-trash icon--left"></span>Delete</a>
                    </div>
                <?php endif ?>
            </li>
        <?php endforeach ?>
    </ul>
<?php else: ?>
    <h2 class="no-video">No videos available</h2>
<?php endif ?>