<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Comment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form search">
    <?php $form = ActiveForm::begin([
        'action' => ['video/search'],
        'method' => 'get',
    ]); ?>
    <div class="search__inner">
        <div class="form-group">
            <label class="control-label" for="search">Search</label>
            <input type="text" class="form-control" name="query" id="search" value="<?php if (isset($_GET['query'])) echo $_GET['query']; ?>"/>
            <?= Html::submitButton('Search', ['class' => 'btn btn--small btn--blue']) ?>
        </div>
    </div>
    <?php $form = ActiveForm::end(); ?>
</div>

