<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Comment */
?>
<?php if (!is_null($comments) && !empty($comments) && count($comments)): ?>
    <ul class="list-comments">
    <?php foreach($comments as $comment):?>
        <?php
            // user of video
            foreach ($users as $user) {
                if ($user->user_id == $comment->user_id) {
                    $comment_user = '<a href="/user/'.$user->user_id.'">'.$user->firstname.' '.$user->lastname.' ('.$user->username.')</a>' ;
                    break;
                }
            }
        ?>
        <li id="comment<?= $comment->id; ?>">
            <div class="list-comments__head">
                <h3><?= $comment_user; ?></h3>
                <div class="list-comments__date"><?= \Yii::$app->formatter->asDatetime($comment->time, "php:d/m/Y H:i"); ?></div>
            </div>
            <div class="list-comments__content">
                <?php if(Yii::$app->user->identity->isAdmin() || Yii::$app->user->identity->id == $comment->user_id): ?>
                    <div class="list-comments__buttons">
                        <?php if(Yii::$app->user->identity->id == $comment->user_id): ?>
                            <a href="/comment/update/<?= $comment->id ?>" class="btn btn--small btn--orange" data-note="Edit"><span class="icon icon-listing-option"></span></a>
                        <?php endif ?>
                        <?php if(Yii::$app->user->identity->isAdmin() || Yii::$app->user->identity->id == $comment->user_id): ?>
                            <a href="/comment/delete/<?= $comment->id ?>" class="btn btn--small btn--red" data-note="Delete"><span class="icon icon-trash"></span></a>
                        <?php endif ?>
                    </div>
                <?php endif ?>
                <p><?= $comment->content; ?></p>
            </div>
        </li>
    <?php endforeach ?>
    </ul>
<?php endif ?>
