<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Comment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'content')->textArea(['maxlength' => true]) ?>

    <div class="form__control form__control--left">
        <?= Html::submitButton($model->isNewRecord ? 'Submit comment' : 'Update', ['class' => 'btn btn--small btn--blue']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>

