<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Comment */
?>


<?= $this->render('_form', [
    'model' => $model,
    'video' => $video
]) ?>

